//
//  BomberMan.m
//  BomberMan
//
//  Created by Shahab Ejaz on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "BomberMan.h"
#import "GameLayer.h"
#import "GameOverScene.h"


int const kPixelPadding = 10;
@implementation BomberMan

@synthesize direction = _direction;
@synthesize gameLayer = _gameLayer;

-(BOOL) outOfBound:(CGPoint)newPlayerPosition {
    
   // CCLOG(@"NEwBomberMAnPosition: %f,%f",newPlayerPosition.x,newPlayerPosition.y);

    if(newPlayerPosition.x<=0|newPlayerPosition.y<=10|newPlayerPosition.x>=470|newPlayerPosition.y>=320) {
        return true;
    }
    else {
        
        return false;
    }
}
-(void) setBomberManPosition:(CGPoint)newPosition {
    
    if([self outOfBound:newPosition]){
        return;
    }
    _newBomberManPosition = newPosition;
    
    switch (self.direction) {
        case Right:
            [self schedule:@selector(bmWalkRight:) interval:0.01];
            break;
        case Left:
            [self schedule:@selector(bmWalkLeft:) interval:0.01];
            break;
        case Up:
            [self schedule:@selector(bmWalkUp:) interval:0.01];
            break;
        case Down:
            [self schedule:@selector(bmWalkDown:) interval:0.01];
            break;
        default:
            break;
    }
}
#pragma Mark BomberMan Walking
-(void) bmWalkRight:(ccTime) dt {
    if ([self outOfBound:_newBomberManPosition]) {
        [self unschedule:@selector(bmWalkRight:)];
        return;
    }
    CGPoint checkForPosition = CGPointMake(_newBomberManPosition.x+kPixelPadding, _newBomberManPosition.y);
    CGPoint tileCoord = [_gameLayer tileCoordForPosition:checkForPosition];
    int tileGID = [_gameLayer.meta tileGIDAt:tileCoord];
    if(tileGID) {
        
        NSDictionary *tileProperties = [_gameLayer.level1 propertiesForGID:tileGID];
        if(tileProperties) {
            NSString *collision = [tileProperties valueForKey:@"Collidable"];
          
            NSString *endGame = [tileProperties valueForKey:@"EndGame"];
            NSString *collectible = [tileProperties valueForKey:@"Collectable"];
            
            if (collision && [collision compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkRight:)];
                return;
            }
            else if (collectible && [collectible compare:@"true"] == NSOrderedSame) {
                
                [_gameLayer.meta removeTileAt:tileCoord];
                [_gameLayer.foreground removeTileAt:tileCoord];
            }

            else if (endGame && [endGame compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkRight:)];
                GameOverScene *gameOverScene = [GameOverScene node];
                [gameOverScene.gameOverLayer.gameOverLabel setString:@"You Win"];
                [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInB transitionWithDuration:1 scene:gameOverScene]];
            }
                    }
    }
    
    self.position = _newBomberManPosition;
    _newBomberManPosition.x += 1;
}
-(void) bmWalkLeft:(ccTime) dt {
    if([self outOfBound:_newBomberManPosition]) {
        [self unschedule:@selector(bmWalkLeft:)];
        return;
    }
    CGPoint checkForPosition = CGPointMake(_newBomberManPosition.x-kPixelPadding, _newBomberManPosition.y);
    CGPoint tileCoord = [_gameLayer tileCoordForPosition:checkForPosition];
    int tileGID = [_gameLayer.meta tileGIDAt:tileCoord];
    if(tileGID) {
        
        NSDictionary *tileProperties = [_gameLayer.level1 propertiesForGID:tileGID];
        if(tileProperties) {
            NSString *collision = [tileProperties valueForKey:@"Collidable"];
            NSString *endGame = [tileProperties valueForKey:@"EndGame"];
            NSString *collectible = [tileProperties valueForKey:@"Collectable"];

            
            if(collision && [collision compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkLeft:)];
                return;
            }
            else if (collectible && [collectible compare:@"true"] == NSOrderedSame) {
                
                [_gameLayer.meta removeTileAt:tileCoord];
                [_gameLayer.foreground removeTileAt:tileCoord];
            }
            else if(endGame && [endGame compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkLeft:)];
                GameOverScene *gameOverScene = [GameOverScene node];
                [gameOverScene.gameOverLayer.gameOverLabel setString:@"You Win"];
                CCTransitionSplitRows * sceneTransition = [CCTransitionSplitRows transitionWithDuration:1 scene:gameOverScene];
                [[CCDirector sharedDirector] replaceScene:sceneTransition];
            }
        }
    }
    
    self.position = _newBomberManPosition;
    _newBomberManPosition.x -= 1;
}
-(void) bmWalkUp:(ccTime) dt {
    if([self outOfBound:_newBomberManPosition]) {
        [self unschedule:@selector(bmWalkUp:)];
        return;
    }
    CGPoint checkForPosition = CGPointMake(_newBomberManPosition.x, _newBomberManPosition.y+kPixelPadding);
    CGPoint tileCoord = [_gameLayer tileCoordForPosition:checkForPosition];
    int tileGID = [_gameLayer.meta tileGIDAt:tileCoord];
    if(tileGID) {
        
        NSDictionary *tileProperties = [_gameLayer.level1 propertiesForGID:tileGID];
        if(tileProperties) {
            
            NSString *collision = [tileProperties valueForKey:@"Collidable"];
            NSString *collectible = [tileProperties valueForKey:@"Collectable"];

            NSString *endGame = [tileProperties valueForKey:@"EndGame"];
            
            if(collision && [collision compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkUp:)];
                return;
            }
            else if (collectible && [collectible compare:@"true"] == NSOrderedSame) {
                
                [_gameLayer.meta removeTileAt:tileCoord];
                [_gameLayer.foreground removeTileAt:tileCoord];
            }
            else if(endGame && [endGame compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkUp:)];
                GameOverScene *gameOverScene = [GameOverScene node];
                [gameOverScene.gameOverLayer.gameOverLabel setString:@"You Win"];
                [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInB transitionWithDuration:1 scene:gameOverScene]];
            }
        }
    }
    
    self.position = _newBomberManPosition;
    _newBomberManPosition.y += 1;
}
-(void) bmWalkDown:(ccTime) dt {
    if([self outOfBound:_newBomberManPosition]) {
        [self unschedule:@selector(bmWalkDown:)];
        return;
    }
    CGPoint checkForPosition = CGPointMake(_newBomberManPosition.x, _newBomberManPosition.y-kPixelPadding);
    CGPoint tileCoord = [_gameLayer tileCoordForPosition:checkForPosition];
    int tileGID = [_gameLayer.meta tileGIDAt:tileCoord];
    if(tileGID) {
        
        NSDictionary *tileProperties = [_gameLayer.level1 propertiesForGID:tileGID];
        if(tileProperties) {
           // CCLOG(@"tilePoperties%@",tileProperties);
            NSString *collision = [tileProperties valueForKey:@"Collidable"];
            NSString *endGame = [tileProperties valueForKey:@"EndGame"];
            NSString *collectible = [tileProperties valueForKey:@"Collectable"];
            if(collision && [collision compare:@"true"] == NSOrderedSame) {
                [self unschedule:@selector(bmWalkDown:)];
                return;
            }
            else if (collectible && [collectible compare:@"true"] == NSOrderedSame) {
                
                [_gameLayer.meta removeTileAt:tileCoord];
                [_gameLayer.foreground removeTileAt:tileCoord];
                _gameLayer.keyCollected = YES;
            }
            else if(endGame && [endGame compare:@"true"] == NSOrderedSame) {
                //[self.gameLayer removeAllChildrenWithCleanup:YES];
                //[self.gameLayer release];
                [self unschedule:@selector(bmWalkDown:)];
                GameOverScene *gameOverScene = [GameOverScene node];
                [gameOverScene.gameOverLayer.gameOverLabel setString:@"You Win"];
                [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInB transitionWithDuration:1 scene:gameOverScene]];
            }
        }
    }
    
    self.position = _newBomberManPosition;
    _newBomberManPosition.y -= 1;
}


@end
