//
//  BomberMan.h
//  BomberMan
//
//  Created by Shahab Ejaz on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

extern int const kPixelPadding;
typedef enum {
    Right,
    Left,
    Up,
    Down
    
}BomberManDirection;
@class GameLayer;
@interface BomberMan : CCSprite {
    
    BomberManDirection _direction;
    CGPoint _newBomberManPosition; 
    GameLayer *_gameLayer;
}
@property (nonatomic) BomberManDirection direction; 
@property (nonatomic, assign) GameLayer *gameLayer;

-(BOOL) outOfBound:(CGPoint)newPlayerPosition;
-(void) setBomberManPosition:(CGPoint)newPosition;
-(void) bmWalkDown:(ccTime) dt;
-(void) bmWalkUp:(ccTime) dt;
-(void) bmWalkLeft:(ccTime) dt;
-(void) bmWalkRight:(ccTime) dt;

@end
