//
//  PowerUp.h
//  BomberMan
//
//  Created by SHAHAB EJAZ on 10/21/12.
//  Copyright (c) 2012 Hardcoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

typedef enum {
    
    kPowerTypeBombRange  = 0,
    kPowerTypeSpeed,
    kPowerTypeSlowEnemy
    
}PowerType;

typedef enum {
    
    kVisiblityDurationTwoSecond = 2,
    kVisibilityDurationThreeSeconds = 3,
    kVisiblityDurationFourSeconds = 4
}VisibiltyDuration;


@interface PowerUp : CCSprite {
    
    
    PowerType _powerType;
    VisibiltyDuration _visibilityDuration;
}

@property (nonatomic, readwrite) PowerType powerType;
@property (nonatomic, readwrite) VisibiltyDuration  visibilityDuration;
    


@end
