//
//  Bomb.m
//  BomberMan
//
//  Created by Mubashir Ismail on 5/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Bomb.h"

//#define Default_Bomb_Range      1

@implementation Bomb

@synthesize bombType = _bombType;
@synthesize bombRange = _bombRange;

-(id) init {
    if (self = [super init]) {
        _bombType = kBombTypeFire;
        _bombRange = kBombRangeLow;
    }
    return self;
}

-(void) dealloc {

    //no need to release primitive data types
//    _bombType = 0;
//    _bombType = 0;

    [super dealloc];
}

-(void) explode {
    
    
}

@end
