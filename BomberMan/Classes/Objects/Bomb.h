//
//  Bomb.h
//  BomberMan
//
//  Created by Mubashir Ismail on 5/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum {
    kBombTypeFire = 0,
    kBombTypeWater,
    kBombTypeElectricity
}BombType;

typedef enum {
    
    kBombRangeLow = 0,
    kBombRAngeMedium,
    kBombRangeHigh
}BombRange;

@interface Bomb : CCSprite {
    BombType        _bombType;
    BombRange       _bombRange;
}

/**
 *	@description	Will tell which type of explosion_particles to show on bomb explosion
 */
@property (nonatomic,readwrite) BombType bombType;

@property (nonatomic, readwrite) BombRange bombRange;


-(void) explode;

@end
