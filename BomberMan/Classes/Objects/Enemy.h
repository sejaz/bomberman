//
//  Enemy.h
//  BomberMan
//
//  Created byShahab Ejaz on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum {
    
    kEnemyTypeMotorCycle = 0,
    kEnemyTypeCar,
    kEnemyTpeBus,
    KEnemyTypeMixed
    
}EnemyType;

typedef enum {
    
    kEnemySpeedSlow = 0,
    kEnemySpeedNormal,
    kEnemySpeedHigh 
} EnemySpeed;

@interface Enemy : CCSprite {
    
    EnemyType _enemyType;
    EnemySpeed _enemySpeed;
}

@property (nonatomic, readwrite) EnemyType enemyType;
@property (nonatomic, readwrite) EnemySpeed enemySpeed;
@end
