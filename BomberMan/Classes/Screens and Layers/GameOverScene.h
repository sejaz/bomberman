//
//  GameOverScene.h
//  BomberMan
//
//  Created byShahab Ejaz on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface GameOverLayer : CCLayer {

    CCLabelTTF * _gameOverLabel;
    
}
@property (nonatomic, assign) CCLabelTTF * gameOverLabel;

@end


@interface GameOverScene : CCScene {
    
    GameOverLayer * _gameOverLayer;
}
@property (nonatomic, assign) GameOverLayer * gameOverLayer;

@end
