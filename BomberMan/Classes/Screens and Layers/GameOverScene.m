//
//  GameOverScene.m
//  BomberMan
//
//  Created byShahab Ejaz on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "GameOverScene.h"
#import "GameLayer.h"

@implementation GameOverScene

@synthesize gameOverLayer = _gameOverLayer;


-(id) init {
    
    self = [super init];
    if(self) {
        
        self.gameOverLayer  = [GameOverLayer node];
        [self addChild:_gameOverLayer];
        
    }
    return self;
}



@end


@implementation GameOverLayer

@synthesize gameOverLabel = _gameOverLabel;
-(void) restart {
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInL transitionWithDuration:1 scene:[GameLayer scene]]];
    
}

-(id) init {
    
    self = [super init];
    if (self) {
        CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);

        CGSize size = [[CCDirector sharedDirector] winSize];
        self.gameOverLabel = [CCLabelTTF labelWithString:@"" fontName:@"Arial" fontSize:24];
        _gameOverLabel.color = ccc3(255, 255,255 );
        _gameOverLabel.position = ccp(size.width/2, size.height/2);
        [self addChild:_gameOverLabel];
        [self performSelector:@selector(restart) withObject:nil afterDelay:3];
    }
    return self;
}

-(void) dealloc {
    
    self.gameOverLabel = nil;
    CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);

    [super dealloc];
}



@end
