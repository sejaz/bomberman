//
//  GameLayer.m
//  BomberMan
//
//  Created byShahab Ejaz on 5/11/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "GameLayer.h"
#import "GameOverScene.h"

// GameLayer implementation
@implementation GameLayer

@synthesize level1 = _level1;
//@synthesize background = _background;
@synthesize bomberMan = _bomberMan;
@synthesize meta = _meta;
@synthesize bombs = _bombs;
@synthesize explosion = _explosion;
@synthesize enemies = _enemies;
@synthesize enemiesToRemove = _enemiesToRemove;
@synthesize foreground = _foreground;
@synthesize keyCollected;
@synthesize flamePositions = _flamePositions;
@synthesize reward = _reward;

#pragma mark - Initialization

static int kpixelpadding = 20;
static int kbombRange = 1;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GameLayer *layer = [GameLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
    
    layer.bombs =  [CCArray array];
    layer.explosion = [CCArray array];
    layer.enemiesToRemove = [CCArray array];
    layer.bomberMan.gameLayer = layer;
    layer.keyCollected = NO;
    layer.flamePositions = [CCArray  array];
    
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
        
        CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
        _level1 = [CCTMXTiledMap tiledMapWithTMXFile:@"Level1.tmx"];
        
        //_background = [_level1 layerNamed:@"Background"];
        
        _meta = [_level1 layerNamed:@"Meta"];
        
        _reward = [CCSprite spriteWithFile:@"thunder.png"];
        
        _powerUpTaken = NO;
        
        _bomberMan = [BomberMan spriteWithFile:@"bomberman.png"];
        
        //_foreground =[_level1 layerNamed:@"Foreground"];
        
        [self addChild:_level1 z:-1];
        
        CCTMXObjectGroup * objects = [_level1 objectGroupNamed:@"Objects"];
        
        NSMutableArray *allObjects = [objects objects];
        NSAssert(allObjects != nil, @"Objects not Found");
        NSMutableDictionary *spawnPoint =nil;
        int x;
        int y;
        self.enemies =[CCArray array];
        
        for (spawnPoint in allObjects) {
            
            x = [[spawnPoint valueForKey:@"x"] intValue];
            y = [[spawnPoint valueForKey:@"y"] intValue];
            if([[spawnPoint valueForKey:@"name"] isEqualToString:@"SpawnPoint"]) {
                _bomberMan.position = ccp(x, y);
                [self addChild:_bomberMan z:1];
            }
            else if([[spawnPoint valueForKey:@"name"] isEqualToString:@"EnemySpawn"]) {
                if([[spawnPoint valueForKey:@"enemy"] intValue] == 1) {
                    
                    int finalY = [[spawnPoint valueForKey:@"eY"] intValue];
                    [self addEnemyAtPoint:CGPointMake(x, y) destination:CGPointMake(x, finalY)];
                }
                else if([[spawnPoint valueForKey:@"enemy"] intValue] == 2) {
                    
                    int finalY = [[spawnPoint valueForKey:@"eY"] intValue];
                    [self addEnemyAtPoint:CGPointMake(x, y) destination:CGPointMake(x, finalY)];
                }
                else if([[spawnPoint valueForKey:@"enemy"] intValue] == 3) {
                    
                    int finalX = [[spawnPoint valueForKey:@"eX"] intValue];
                    [self addEnemyAtPoint:CGPointMake(x, y) destination:CGPointMake(finalX, y)];
                }
                else if ([[spawnPoint valueForKey:@"enemy"] intValue] == 4) {
                    
                    int finalX = [[spawnPoint valueForKey:@"eX"] intValue];
                    [self addEnemyAtPoint:CGPointMake(x, y) destination:CGPointMake(finalX, y)];
                }
                
                
            }
            else if ([[spawnPoint valueForKey:@"name"] isEqualToString:@"RewardSpawn"]) {
                
                int a = [[spawnPoint valueForKey:@"x"] intValue];
                int b = [[spawnPoint valueForKey:@"y"] intValue];
                _reward.position = ccp(a,b);
                [self addChild:_reward z:1];
                
                
                
            }
        }
        [self scheduleUpdate];
        self.isTouchEnabled = YES;
        _meta.visible = NO;
	}
  
    UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeDetected:)];
    swipeUpDown.direction = UISwipeGestureRecognizerDirectionDown;
    swipeUpDown.cancelsTouchesInView =  NO;
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:swipeUpDown];
    [swipeUpDown release];
    
    UISwipeGestureRecognizer *swipeDownUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeDetected:)];
    swipeDownUp.direction = UISwipeGestureRecognizerDirectionUp;
    swipeDownUp.cancelsTouchesInView =  NO;
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:swipeDownUp];
    [swipeDownUp release];
    
    UISwipeGestureRecognizer *swipeRightLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeDetected:)];
    swipeRightLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRightLeft.cancelsTouchesInView =  NO;
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:swipeRightLeft];
    [swipeRightLeft release];
    
    UISwipeGestureRecognizer *swipeLeftRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(swipeDetected:)];
    swipeLeftRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeLeftRight.cancelsTouchesInView =  NO;
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:swipeLeftRight];
    [swipeLeftRight release];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DoubleTapDetected:)];
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.numberOfTouchesRequired = 1;
    [[[CCDirector sharedDirector] openGLView] addGestureRecognizer:doubleTap];
    
    [doubleTap release];
    
	return self;
}

-(void) detectBomberManCollisionWithMonster {
    
    
}

-(void) detectBombsCollisionWithMonster:(ccTime)interval {
    
    for (CCSprite * enemy in _enemies) {
        
       // NSLog(@"ENEMY x: %f y: %f", enemy.position.x,enemy.position.y);

        for (CCSprite * explodedBomb in _explosion) {
            
         //   NSLog(@"FLAMES x: %f y: %f", explodedBomb.position.x,explodedBomb.position.y);
        
            if (CGRectIntersectsRect([explodedBomb boundingBox],[enemy boundingBox])) {
                [_enemiesToRemove addObject:enemy];
                [self removeChild:enemy cleanup:YES];
            }
        }
    }
    
    //after completely checking the explosion unschedule the selector
   // [self unschedule:@selector(detectBombsCollisionWithMonster:)];
}

-(void) remove:(ccTime)interval {
    [self unschedule:@selector(detectBombsCollisionWithMonster:)];
    for (CCSprite *flame in _explosion) {
        
        [self removeChild:flame cleanup:YES];
    }
    //CCSprite *explosion = [_explosion objectAtIndex:0];
    //[_explosion removeObject:explosion];
    [_explosion removeAllObjects];
    //[self removeChild:explosion cleanup:YES];
}

-(void) placeBomb {
    Bomb *bomb = [Bomb spriteWithFile:@"Bomb.png"];
    bomb.position = _bomberMan.position;
    bomb.bombType = kBombTypeFire;
    bomb.bombRange = kbombRange;
    [_bombs addObject:bomb];
    [self addChild:bomb];
    [self performSelector:@selector(explode:) withObject:nil afterDelay:1];
}

-(void) explode:(ccTime)interval {
    Bomb * bomb = [_bombs objectAtIndex:0];
    self.flamePositions = [CCArray array];
    int pixelPadding = kpixelpadding;
    for (int i = 0; i < bomb.bombRange; i++) {
        
         //add a flame position to left
        CGPoint leftPosition = CGPointMake(bomb.position.x - pixelPadding, bomb.position.y);
        NSValue *leftValue = [NSValue valueWithCGPoint:leftPosition]; 
        NSLog(@"%@",leftValue);
        [_flamePositions addObject:leftValue];
        //add a flame position to right
        CGPoint rightPosition = CGPointMake(bomb.position.x + pixelPadding, bomb.position.y);
        NSValue *rightNumber = [NSValue valueWithCGPoint:rightPosition]; 
        [_flamePositions addObject:rightNumber];
        //add a flame position to up
        CGPoint upPosition = CGPointMake(bomb.position.x, bomb.position.y + pixelPadding);
        NSValue *upValue = [NSValue valueWithCGPoint:upPosition]; 
        [_flamePositions addObject:upValue];
        //add a flame position to left
        CGPoint downPosition = CGPointMake(bomb.position.x, bomb.position.y - pixelPadding);
        NSValue *downValue = [NSValue valueWithCGPoint:downPosition]; 
        [_flamePositions addObject:downValue];
        
        pixelPadding = pixelPadding +20;
        
    }
    for (NSValue * position in _flamePositions) {
        
        CCSprite *explode = [CCSprite spriteWithFile:@"flame.png"];
        CGPoint pos = [position CGPointValue];
        explode.position = pos;
        [self addChild:explode];
        [_explosion addObject:explode];
        
    }
    [self removeChild:bomb cleanup:YES];
    [_bombs removeObject:bomb];
    [self schedule:@selector(detectBombsCollisionWithMonster:) interval:0];
    NSLog(@"AddExplosion: %i",[_explosion count]);
    [self performSelector:@selector(remove:) withObject:nil afterDelay:2];
    
}

-(void) addEnemyAtPoint:(CGPoint)enemyPoint destination:(CGPoint)finalPoint {
    
    Enemy *enemy = [Enemy spriteWithFile:@"monster.png"];
    enemy.position = enemyPoint;
    [self addChild:enemy];
    [_enemies addObject:enemy];
    
    id moveForward = [CCMoveTo actionWithDuration:2 position:finalPoint];
    id moveBack = [CCMoveTo actionWithDuration:2 position:enemyPoint];
    [enemy runAction:[CCRepeatForever actionWithAction:[CCSequence actions:moveForward,moveBack, nil]]];
    
}

#pragma mark - UIGestures Methods

-(void) DoubleTapDetected:(UITapGestureRecognizer *)gesture {
    
    [self placeBomb];
    
}

-(void) tapDetected:(UITapGestureRecognizer *)gesture {
    
    [_bomberMan unschedule:@selector(bmWalkLeft:)];
    [_bomberMan unschedule:@selector(bmWalkRight:)];
    [_bomberMan unschedule:@selector(bmWalkUp:)];
    [_bomberMan unschedule:@selector(bmWalkDown:)];
    
}

-(void) swipeDetected:(UISwipeGestureRecognizer *)gesture {
    
    [_bomberMan unschedule:@selector(bmWalkLeft:)];
    [_bomberMan unschedule:@selector(bmWalkRight:)];
    [_bomberMan unschedule:@selector(bmWalkUp:)];
    [_bomberMan unschedule:@selector(bmWalkDown:)];
    switch (gesture.direction) {
        case UISwipeGestureRecognizerDirectionUp:
            NSLog(@"Moved Up");
            _newBMPosition = ccp(_bomberMan.position.x, _bomberMan.position.y + 1);
            [_bomberMan setDirection:Up];
            break;
        case UISwipeGestureRecognizerDirectionDown:
            NSLog(@"Moved Down");
            _newBMPosition = ccp(_bomberMan.position.x, _bomberMan.position.y - 1);
            [_bomberMan setDirection:Down];
            break;
        case UISwipeGestureRecognizerDirectionLeft:
            NSLog(@"Moved Left");
            _newBMPosition = ccp(_bomberMan.position.x - 1, _bomberMan.position.y);
            [_bomberMan setDirection:Left];
            break;
        case UISwipeGestureRecognizerDirectionRight:
            NSLog(@"Moved Right");
            _newBMPosition = ccp(_bomberMan.position.x +1, _bomberMan.position.y);
            [_bomberMan setDirection:Right];
            break;
        default:
            break;
            
    }
    [_bomberMan setBomberManPosition:_newBMPosition];
    
}

-(void) update:(ccTime) delta {
    // Remove dead Enemies first if any
    if([_enemiesToRemove count]) {
        
        [_enemies removeObjectsInArray:_enemiesToRemove];
        [_enemiesToRemove removeAllObjects];
    }
    // BOOL breakOuterLoop = NO;
    
    //CGRect bomberManRect = CGRectMake(_bomberMan.position.x, _bomberMan.position.y, _bomberMan.contentSize.width, _bomberMan.contentSize.height);
    //NSLog(@"MonsterCount: %i",[_monsters count]);
    for (CCSprite * enemy in _enemies) {
        // CGRect enemyRect = CGRectMake(enemy.position.x, enemy.position.y, enemy.contentSize.width, enemy.contentSize.height);
        if(CGRectIntersectsRect([_bomberMan boundingBox], [enemy boundingBox])) {
            [self unscheduleAllSelectors];
            [self.bomberMan unscheduleAllSelectors];
            GameOverScene *gameOverScene = [GameOverScene node];
            [gameOverScene.gameOverLayer.gameOverLabel setString:@"You Lose"];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInB transitionWithDuration:1 scene:gameOverScene]];
        }
    }
    if (!_powerUpTaken) {
        if (CGRectIntersectsRect([_reward boundingBox], [_bomberMan boundingBox])) {
            NSLog(@"POWER UP TAKEN...");
            [self removeChild:_reward cleanup:YES];
            _powerUpTaken = YES;
            kbombRange = 2;
            
        }
    }
    
}

-(CGPoint) tileCoordForPosition:(CGPoint)newPosition {
    
    int x = newPosition.x/_level1.tileSize.width;
    int y = ((_level1.mapSize.height * _level1.tileSize.height) - newPosition.y)/_level1.tileSize.height;
    
    return ccp(x, y);
    
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
    CCLOG(@"%@: %@", NSStringFromSelector(_cmd), self);
    [self.bombs removeAllObjects];
    self.bombs = nil;
    [self.explosion removeAllObjects];
    self.explosion = nil;
    [self.enemies removeAllObjects];
    self.enemies = nil;
    [self.enemiesToRemove removeAllObjects];
    self.enemiesToRemove = nil;
	[super dealloc];
}
@end
