//
//  GameLayer.h
//  BomberMan
//
//  Created by Mubashir Ismail on 5/11/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"

// GameLayer
@interface GameLayer : CCLayer<UIGestureRecognizerDelegate> {
    
    CCTMXTiledMap *         _level1;
    //CCTMXLayer *            _background;
    BomberMan *             _bomberMan;
    CCTMXLayer *            _meta;
    CCArray *               _bombs;
    CCArray *               _explosion;
    CCArray *               _enemies;
    CCArray *               _enemiesToRemove;
    CGPoint                 _newBMPosition;
    CCTMXLayer *            _foreground;
    BOOL                    _keyCollected;
    CCArray *               _flamePositions;
    CCSprite *              _reward;
    BOOL                    _powerUpTaken;
}

@property (nonatomic, assign) BomberMan *       bomberMan;
@property (nonatomic, assign) CCTMXTiledMap *   level1;
//@property (nonatomic, assign) CCTMXLayer *      background;
@property (nonatomic, assign) CCTMXLayer *      meta;
@property (nonatomic, retain) CCArray *         bombs;
@property (nonatomic, retain) CCArray *         explosion;
@property (nonatomic, retain) CCArray *         enemies;
@property (nonatomic, retain) CCArray *         enemiesToRemove;
@property (nonatomic, assign) CCTMXLayer *      foreground;
@property (nonatomic) BOOL                      keyCollected;
@property (nonatomic, assign) CCArray *         flamePositions;
@property (nonatomic, assign) CCSprite *        reward;
-(CGPoint) tileCoordForPosition:(CGPoint)newPosition;

-(void) placeBomb;

-(void)explode:(ccTime)interval;

// returns a CCScene that contains the GameLayer as the only child
+(CCScene *) scene;

@end
