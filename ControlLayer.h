//
//  ControlLayer.h
//  Tutorial1
//
//  Created by Alex Brown on 29/06/2011.
//  Copyright 2011 none. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SneakyJoystick.h"
#import "SneakyExtensions.h"
#import "SneakyJoystickSkinnedBase.h"

#import "SneakyButton.h"


@interface ControlLayer : CCLayer {
    
    //create an instance of the SneakyJoystick class
    SneakyJoystick* joystick;
    //create an instance of the SneakyJoystickSkinnedBase class
    SneakyJoystickSkinnedBase* JSSkin;
    
    SneakyButton* button;
    
    SneakyButtonSkinnedBase* BSkin;
    
    ccTime totalTime;
    ccTime nextShotTime;
}

-(id)initWithLayer;
+(id)control;
-(void)addJoystick;
-(void)addButton;

@end
